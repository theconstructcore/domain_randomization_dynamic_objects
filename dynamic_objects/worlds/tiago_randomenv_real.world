<?xml version="1.0" ?>
<sdf version="1.4">
  <world name="default">
    <scene>
        <shadows>1</shadows>
    </scene>

    <physics type="ode">
      <gravity>0 0 -9.81</gravity>
      <ode>
        <solver>
          <type>quick</type>
          <iters>20</iters>
          <sor>1.0</sor>
        </solver>
        <constraints>
          <cfm>0.0</cfm>
          <erp>0.2</erp>
          <contact_max_correcting_vel>100.0</contact_max_correcting_vel>
          <contact_surface_layer>0.0</contact_surface_layer>
        </constraints>
      </ode>
      <real_time_update_rate>1000</real_time_update_rate>
      <max_step_size>0.001</max_step_size>
    </physics>

    <light type="directional" name="light1">
      <cast_shadows>true</cast_shadows>
      <pose>-2 0 4 0 0 0</pose>
      <diffuse>1.0 1.0 1.0 1</diffuse>
      <specular>0.1 0.1 0.1 1</specular>
      <attenuation>
        <range>20</range>
        <constant>0.9</constant>
        <linear>0.01</linear>
        <quadratic>0.001</quadratic>
      </attenuation>
      <direction>0.2 0.0 -1.0</direction>
    </light>

    <light type="directional" name="light2">
      <cast_shadows>true</cast_shadows>
      <pose>2 0 4 0 0 0</pose>
      <diffuse>1.0 1.0 1.0 1</diffuse>
      <specular>0.1 0.1 0.1 1</specular>
      <attenuation>
        <range>20</range>
        <constant>0.9</constant>
        <linear>0.01</linear>
        <quadratic>0.001</quadratic>
      </attenuation>
      <direction>0.2 0.0 -1.0</direction>
    </light>


    <!-- A ground plane -->
    <include>
      <uri>model://ground_plane</uri>
    </include>

     <!-- A table -->
    <include>
      <uri>model://table_2m</uri>
      <name>table1</name>
      <pose>1.45267 0 0 0 0 3.14</pose>
    </include>

    <!-- Objects -->

    <include>
      <uri>model://coffee_cup</uri>
      <name>coffee_cup1</name>
      <pose>0.531465000565 0.0364408006101 0.84 0 0 0</pose>
    </include>

    <include>
      <uri>model://coffe_mug</uri>
      <name>coffe_mug1</name>
      <pose>0.752354938291 0.255232430183 0.84 0 0 0</pose>
    </include>

    <include>
      <uri>model://saucer</uri>
      <name>saucer1</name>
      <pose>0.725436755984 0.0774224373658 0.84 0 0 0</pose>
    </include>

    <include>
      <uri>model://coffe_cup_with_saucer</uri>
      <name>coffe_cup_with_saucer1</name>
      <pose>0.819465881623 -0.306091246488 0.84 0 0 0</pose>
    </include>

    <include>
      <uri>model://laptop</uri>
      <name>laptop1</name>
      <pose>1.09159217865 0.226752690327 0.84 0 0 -1.59</pose>
    </include>

    <include>
      <uri>model://cup_cake</uri>
      <name>cup_cake1</name>
      <pose>1.21234958478 -0.337946661567 0.84 0 0 0</pose>
    </include>

    <include>
      <uri>model://coffee_spoon</uri>
      <name>coffee_spoon1</name>
      <pose>0.595837738643 -0.24561250758 0.84 0 0 -0.721349</pose>
    </include>

    <include>
      <uri>model://fork</uri>
      <name>fork1</name>
      <pose>1.08215120808 -0.118354249596 0.84 0 0 1.690194</pose>
    </include>


    <gui fullscreen="0">
      <camera name="user_camera">
        <pose>2.213458 0.523699 1.669535 -0.000001 0.535639 -2.786999</pose>
        <view_controller>orbit</view_controller>
      </camera>
    </gui>

    <!-- reference to your plugin -->
    <plugin name="dynamic_world_plugin" filename="libdynamic_world_plugin.so">

      <log_level>WARN</log_level>
      <update_frequency>1.0</update_frequency>

      <model_to_track_name>coffee_cup1</model_to_track_name>

      <model_to_ignore_randcolours>tiago_steel</model_to_ignore_randcolours>
      <model_to_ignore_movement>tiago_steel,ground_plane,table1,laptop1</model_to_ignore_movement>

      <x_max>0.829</x_max>
      <x_min>0.53</x_min>
      <y_max>0.24</y_max>
      <y_min>-0.24</y_min>
      <z_max>0.76</z_max>
      <z_min>0.76</z_min>

      <x_max_distractors>1.1</x_max_distractors>
      <x_min_distractors>0.5</x_min_distractors>
      <y_max_distractors>0.35</y_max_distractors>
      <y_min_distractors>-0.35</y_min_distractors>
      <z_max_distractors>0.76</z_max_distractors>
      <z_min_distractors>0.76</z_min_distractors>

      <z_increment_percentage>0.0</z_increment_percentage>


      <roll_max>0.0</roll_max>
      <roll_min>0.0</roll_min>
      <pitch_max>0.0</pitch_max>
      <pitch_min>0.0</pitch_min>
      <yaw_max>0.0</yaw_max>
      <yaw_min>0.0</yaw_min>

      <light_x_max>2.0</light_x_max>
      <light_x_min>-2.0</light_x_min>
      <light_y_max>1.5</light_y_max>
      <light_y_min>-1.5</light_y_min>
      <light_z_max>2.0</light_z_max>
      <light_z_min>2.0</light_z_min>

      <light_roll_max>0.3</light_roll_max>
      <light_roll_min>-0.3</light_roll_min>
      <light_pitch_max>0.7</light_pitch_max>
      <light_pitch_min>-0.7</light_pitch_min>
      <light_yaw_max>0.0</light_yaw_max>
      <light_yaw_min>0.0</light_yaw_min>

      <cam_x_max>-0.215121</cam_x_max>
      <cam_x_min>-0.215121</cam_x_min>
      <cam_y_max>0.3</cam_y_max>
      <cam_y_min>0.3</cam_y_min>
      <cam_z_max>1.092078</cam_z_max>
      <cam_z_min>1.092078</cam_z_min>

      <cam_roll_max>0.0</cam_roll_max>
      <cam_roll_min>0.0</cam_roll_min>
      <cam_pitch_max>0.6991</cam_pitch_max>
      <cam_pitch_min>0.6991</cam_pitch_min>
      <cam_yaw_max>0.0</cam_yaw_max>
      <cam_yaw_min>0.0</cam_yaw_min>

      <alpha>0.5</alpha>
      <beta>0.5</beta>
      <random_distribution_type>beta_distribution</random_distribution_type>

      <colour_randomiser_active>yes</colour_randomiser_active>

    </plugin>

  </world>
</sdf>
